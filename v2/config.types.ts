export interface Services {
    auth: { port: number };
    articles: { port: number };
    storage: { port: number };
    blogs: { port: number };
}

export interface Security {
    secret: string;
    token_exp: string;
    cookie_exp: number;
};

export interface IStorage {
    upload_dir: string;
    max_filesize: number;
    allowed_filetypes: string[];
};

export interface Cloud {
    cloud_name: string;
    api_key: string;
    api_secret: string;
};
