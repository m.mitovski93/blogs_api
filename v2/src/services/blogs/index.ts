import express, { Application } from 'express';
import handlers from './handlers/blogs';
import config from '../../pkg/config';
import { Services } from '../../../config.types';
import { authenticate } from '../../pkg/middleware/index'
const cfg = config.get<Services>('services');

const app: Application = express();
app.use(express.json());
const PORT: number = cfg.blogs.port;

app.post('/api/v2/blogs', authenticate, handlers.create);
app.get('/api/v2/blogs/me', authenticate, handlers.getAllByUser);
app.get('/api/v2/blogs', handlers.getAll);
app.get('/api/v2/blogs/:id', handlers.getOne);
app.put('/api/v2/blogs', authenticate, handlers.update);
app.delete('/api/v2/blogs/:id', authenticate, handlers.remove);
app.delete('/api/v2/blogs/image/:id', authenticate, handlers.deleteImage);

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});