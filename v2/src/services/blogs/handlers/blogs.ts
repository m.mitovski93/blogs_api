import { Request, Response } from 'express';
import Blog from '../../../pkg/blogs';
import sanitazers from '../../../pkg/sanitazers';

const create = async (req: Request, res: Response) => {
    req.body.author_id = res.locals.user.id;
    sanitazers.clear(req.body);
    const b = Blog.fromJson(req.body);
    const err = await b.validate(['create']);
    if (err) {
        return res.status(400).send(err);
    };

    try {
        const blog = await b.create();
        b.id = blog.insertId;
        if (b.images.length > 0) {
            await b.saveImages();
        };
        return res.status(201).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };
};

const update = async (req: Request, res: Response) => {
    req.body.author_id = res.locals.user.id;
    sanitazers.clear(req.body);
    const b = Blog.fromJson(req.body);
    const err = await b.validate(['update']);
    if (err) {
        return res.status(400).send(err);
    };
    try {
        await b.update();
        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };
};

const remove = async (req: Request, res: Response) => {
    try {
        const blogId = parseInt(req.params.id);
        const b = await Blog.delete(blogId, res.locals.user.id);
        if (!b.affectedRows) {
            return res.status(404).send('Not found')
        };
        await Blog.deleteImagesByBlog(blogId, res.locals.user.id)
        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

const getAll = async (req: Request<{}, {}, {}, { page: string }>, res: Response) => {
    let page: number = parseInt(req.query.page);
    let offset: number = (page - 1) * 10;
    let limit: number = 10;
    try {
        const blogs = await Blog.getAll(limit, offset);
        if (blogs.length === 0) {
            return res.status(200).send(blogs);
        };
        const imageIds: number[] = blogs.map(b => b.id);
        const images = await Blog.getImages(imageIds);

        res.status(200).send({
            blogs,
            images
        });
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

const getOne = async (req: Request, res: Response) => {
    try {
        const blogId = parseInt(req.params.id);
        const [blog] = await Blog.getOne(blogId);
        if (!blog) {
            return res.status(404).send('Not found');
        };
        const images = await Blog.getImages([blog.id]);
        res.status(200).send({
            blog,
            images
        });
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

const getAllByUser = async (req: Request<{}, {}, {}, { page: string }>, res: Response) => {
    let page: number = parseInt(req.query.page);
    let offset: number = (page - 1) * 10;
    let limit: number = 10;
    try {
        const blogs = await Blog.getAllByUser(res.locals.user.id, limit, offset);
        if (blogs.length === 0) {
            return res.status(200).send(blogs);
        };
        const imageIds: number[] = blogs.map(b => b.id);
        const images = await Blog.getImages(imageIds);
        res.status(200).send({
            blogs,
            images
        });
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

const deleteImage = async (req: Request<{ id: string }>, res: Response) => {
    try {
        const id = parseInt(req.params.id);
        const i = await Blog.deleteOneImage(id, res.locals.user.id);
        if (!i.affectedRows) {
            return res.status(404).send('Not found')
        };
        res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

export default {
    create,
    update,
    remove,
    getAll,
    getOne,
    getAllByUser,
    deleteImage
}