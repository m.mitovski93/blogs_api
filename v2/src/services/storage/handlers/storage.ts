import { Request, Response } from 'express';
import Storage from '../../../pkg/storage';


const upload = async (req: Request, res: Response) => {
    const storageData = {
        files: res.locals.files,
        userId: res.locals.user.id
    };
    const s = Storage.fromJson(storageData);
    const err = await s.validate();
    if (err) {
        return res.status(400).send(err[0].constraints)
    };

    try {
        s.validateFiles();
        const images = [];
        for (let i = 0; i < s.files.length; i++) {
            const res = await s.uploadToCloud(s.files[i].data);
            const image = {
                public_id: res.public_id,
                url: res.url
            };
            images.push(image);
        };
        res.status(200).send(images);
    } catch (error: any) {
        res.status(400).send(error);
    };
};

const removeMany = async (req: Request, res: Response) => {
    try {
        const d = await Storage.deleteMany(req.body.images);
        return res.status(204).send('ok');
    } catch (error) {
        return res.status(400).send(error);
    };
};

const removeOne = async (req: Request, res: Response) => {
    try {
        const d = await Storage.deleteOne(req.params.id);
        return res.status(204).send('ok');
    } catch (error) {
        return res.status(400).send(error);
    };
};


export default {
    upload,
    removeMany,
    removeOne
};