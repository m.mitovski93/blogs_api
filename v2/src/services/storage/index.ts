import express, { Application } from 'express';
import handlers from './handlers/storage';
import fileUpload, { FileArray } from 'express-fileupload'
import config from '../../pkg/config';
import { Services } from '../../../config.types';
import { authenticate, filesToArr } from '../../pkg/middleware/index'
const cfg = config.get<Services>('services');

const app: Application = express();
app.use(express.json());
app.use(fileUpload());
const PORT: number = cfg.storage.port;

app.post('/api/v2/storage', authenticate, filesToArr, handlers.upload);
app.delete('/api/v2/storage', handlers.removeMany);
app.delete('/api/v2/storage/:id', handlers.removeOne);


app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});