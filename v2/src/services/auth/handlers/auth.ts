import User from '../../../pkg/users';
import { Request, Response } from 'express';
import { generateToken } from '../../../pkg/security/jwt';

const createAccount = async (req: Request, res: Response) => {
    const u = User.fromJson(req.body);
    const err = await u.validate(['create']);
    if (err) {
        return res.status(400).send(err);
    };
    if (u.password !== u.repeatPassword) {
        return res.status(400).send('Passwords do not match');
    };

    try {
        u.hashPassword();
        await u.create();
        res.status(201).send('ok');
    } catch (error: any) {
        if (error.errno === 1062) {
            return res
                .status(400)
                .send("email already in use");
        };
        console.log(error);
        res.send(error);
    };
};

const login = async (req: Request, res: Response) => {
    const u = User.fromJson(req.body);
    try {
        const [user] = await u.getByEmail();
        if (!user) {
            return res.status(400).send('bad request');
        };
        if (!u.verifyPassword(req.body.password, user.password)) {
            return res.status(400).send('bad request');
        }

        const token = generateToken({
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
        });
        return res.status(200).send(token);
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    }
};

const update = async (req: Request, res: Response) => {
    req.body.id = res.locals.user.id;
    const u = User.fromJson(req.body);
    const err = await u.validate(['update']);

    if (err) {
        return res.status(400).send(err);
    };
   
    try {
        const [user] = await User.getById(res.locals.user.id);
        if (!user) {
            return res.status(400).send('bad request');
        };
    
        if (u.email !== user.email) {
            return res.status(400).send('bad request');
        };

        if (u.password !== user.password) {
            u.hashPassword();
        };
        
        await u.update();
        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };

};

const getAuthUser = async (req: Request, res: Response) => {
    try {
        const [u] = await User.getById(res.locals.user.id);
        if (!u) {
            return res.status(401).send('Unauthorized');
        };
        return res.status(200).send(u);
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

const deleteAccount = async (req: Request, res: Response) => {
    try {
        await User.delete(res.locals.user.id);
        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };
};

export default {
    createAccount,
    login,
    deleteAccount,
    getAuthUser,
    update
};