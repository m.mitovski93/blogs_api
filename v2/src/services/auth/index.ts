import express, { Application } from 'express';
import handlers from '../auth/handlers/auth';
import config from '../../pkg/config';
import { Services } from '../../../config.types';
import { authenticate } from '../../pkg/middleware/index'
const cfg = config.get<Services>('services');

const app: Application = express();
app.use(express.json());
const PORT: number = cfg.auth.port;

app.post('/api/v2/auth', handlers.createAccount);
app.get('/api/v2/auth', authenticate, handlers.getAuthUser);
app.post('/api/v2/auth/login', handlers.login);
app.put('/api/v2/auth/update', authenticate, handlers.update);
app.delete('/api/v2/auth/delete', authenticate, handlers.deleteAccount);

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});