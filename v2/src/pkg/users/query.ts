const Queries = {
    create: `
    INSERT INTO users
    (firstName, lastName, email, password)
    VALUES(:firstName,:lastName,:email,:password);
    `,
    getByEmail: `
    SELECT id, firstName, lastName, email, password
    FROM users
    WHERE email = :email
    AND deleted = 0;
    `,
    updateById: `
    UPDATE users SET
    id = :id, 
    firstName = :firstName,
    lastName = :lastName,
    email = :email,
    password = :password,
    created_at = :created_at,
    updated_at = :updated_at,
    deleted = 0
    WHERE id = :id;
   `,
    deleteById: `
    UPDATE users Set 
    deleted = 1
    WHERE id = :id;
    `,
    getById: `
    SELECT id,
    firstName, 
    lastName,
    email,
    password,
    created_at,
    updated_at
    FROM users
    WHERE id = :id 
    AND deleted = 0;
    `
};

export default Queries;