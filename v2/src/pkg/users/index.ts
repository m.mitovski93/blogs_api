import 'reflect-metadata';
import { Expose, plainToClass } from "class-transformer";
import connection from '../db';
import Queries from './query'
import { ResultSetHeader, RowDataPacket } from 'mysql2';
import {
    validate,
    Length,
    IsEmail,
    MinLength,
    IsInt,
    IsString,
    IsDateString,
    Min,
    Max
} from 'class-validator';
import bcrypt from 'bcrypt';
import { queryBuilder } from 'mysql-query-placeholders';


class User {
    @Expose()
    @IsInt({ groups: ['update'] })
    id?: number;

    @Expose()
    @Length(3, 20, {
        groups: ['create', 'update']
    })
    public firstName: string;

    @Expose()
    @IsString()
    @Length(3, 20, {
        groups: ['create', 'update']
    })
    public lastName: string;

    @Expose({ name: 'email' })
    @IsEmail({}, { groups: ['create', 'update'] })
    public email: string;

    @Expose()
    @IsString()
    @MinLength(8, {
        groups: ['create', 'update']
    })
    public password: string;

    @Expose()
    @IsString()
    @MinLength(8, {
        groups: ['create']
    })
    public repeatPassword: string;

    @Expose()
    @IsInt({ groups: ['update'] })
    @Min(0)
    @Max(1)
    public deleted: number;

    @Expose()
    @IsDateString({}, { groups: ['update'] })
    public created_at?: Date;

    @Expose()
    @IsDateString({}, { groups: ['update'] })
    public updated_at?: Date;

    constructor(firstName: string,
        lastName: string,
        email: string,
        password: string,
        repeatPassword: string,
        created_at: Date,
        updated_at: Date,
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.repeatPassword = repeatPassword;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.deleted = 0;
    };

    public async create() {
        const queryData = queryBuilder(Queries.create, this);
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public async getByEmail() {
        const queryData = queryBuilder(Queries.getByEmail, {
            email: this.email
        });

        const [rows] = await connection.promise().query<RowDataPacket[]>(queryData);
        return rows;
    };

    public static async getById(id: number) {
        const queryData = queryBuilder(Queries.getById, {
            id
        });
        const [rows] = await connection.promise().query<RowDataPacket[]>(queryData);
        return rows;
    };

    public async update() {
        const queryData = queryBuilder(Queries.updateById, this);
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public static async delete(id: number) {
        const queryData = queryBuilder(Queries.deleteById, {
            id
        });

        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public hashPassword() {
        const salt = bcrypt.genSaltSync(10);
        const hashPassword = bcrypt.hashSync(this.password, salt);
        this.password = hashPassword;
    };

    public verifyPassword(password: string, hashPassword: string) {
        return bcrypt.compareSync(password, hashPassword);
    };

    public async validate(groups: string[] = []) {
        return validate(this, {
            groups: groups,
            // skipMissingProperties: groups.includes('update') ? true : false,
            forbidUnknownValues: true,
            validationError: { target: false }
        }).then(errors => {
            if (errors.length > 0) {
                return errors
            }
            return null
        });
    };

    static fromJson(data: Object) {
        return plainToClass(this, data, { excludeExtraneousValues: true });
    };
};

export default User;

