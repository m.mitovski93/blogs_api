const clear = (data: any) => {
    for (let key in data) {
        if (typeof data[key] === "string") {
            data[key] = data[key].trim();
        }

        if (data[key] === "") {
            delete data[key];
        }
    }
};

export default  {
    clear,
};
