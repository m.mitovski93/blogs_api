import 'reflect-metadata';
import { Expose, plainToClass } from "class-transformer";
import { v2 as cloudinary, UploadApiResponse, DeleteApiResponse } from 'cloudinary';
import config from '../config';
import { Cloud } from '../../../config.types';
import { Readable } from 'stream';
import { UploadedFile } from 'express-fileupload';
import { ArrayMaxSize, IsInt, validate } from 'class-validator';
import { IStorage } from '../../../config.types';
const cfgCloud = config.get<Cloud>('cloudinary');
const cfgStorage = config.get<IStorage>('storage');

cloudinary.config({
    cloud_name: cfgCloud.cloud_name,
    api_key: cfgCloud.api_key,
    api_secret: cfgCloud.api_secret,
});

class Storage {
    @Expose()
    @ArrayMaxSize(10)
    files: UploadedFile[];

    @Expose()
    @IsInt()
    userId: number;

    constructor(files: UploadedFile[], userId: number) {
        this.files = files;
        this.userId = userId;
    };

    public async uploadToCloud(buffer: Buffer): Promise<UploadApiResponse> {
        return new Promise((resolve, reject) => {
            let stream = cloudinary.uploader.upload_stream({
                // folder: `user_${this.userId}`
            }, (err: any, res: any) => {
                if (err) {
                    reject(err);
                }
                resolve(res);
            })
            const readableStream = Readable.from(buffer);
            readableStream.pipe(stream);
        });
    };

    public static async deleteMany(images: string[]): Promise<DeleteApiResponse> {
        return new Promise((resolve, reject) => {
            cloudinary.api.delete_resources(images, (err, res) => {
                if (err) {
                    return reject(err);
                }
                return resolve(res);
            });
        });
    };

    public static async deleteOne(id: string): Promise<DeleteApiResponse> {
        return new Promise((resolve, reject) => {
            cloudinary.uploader.destroy(id, (err, res) => {
                if (err) {
                    return reject(err);
                }
                return resolve(res);
            });
        });
    };

    public validateFiles() {
        this.files.forEach((file) => {
            if (file.size > cfgStorage.max_filesize) {
                const err: any = new Error()
                err.message = 'file too large'
                err.statusCode = 413;
                throw err;
            };
            if (!cfgStorage.allowed_filetypes.includes(file.mimetype)) {
                const err: any = new Error()
                err.message = 'file type not supported'
                err.statusCode = 415;
                throw err;
            };
        });
    };

    public async validate(groups: string[] = []) {
        return validate(this, {
            groups: groups,
            forbidUnknownValues: true,
            validationError: { target: false }
        }).then(errors => {
            if (errors.length > 0) {
                return errors;
            }
            return null;
        });
    };

    static fromJson(data: Object) {
        return plainToClass(this, data, { excludeExtraneousValues: true });
    };
};

export default Storage;