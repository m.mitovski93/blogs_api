import { Secret, sign, SignOptions, verify, JwtPayload } from 'jsonwebtoken';
import config from "../../config";
import { Security } from "../../../../config.types";

const cfg = config.get<Security>('security');

const generateToken = (payload: JwtPayload): string => {
    const secret: Secret = cfg.secret;
    const options: SignOptions = {
        expiresIn: cfg.token_exp
    };
    return sign(payload, secret, options);
};

const verifyToken = (token: string) => {
    return verify(token, cfg.secret);
};


export {
    generateToken,
    verifyToken
};