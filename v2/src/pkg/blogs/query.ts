const Queries = {
    create: `INSERT INTO 
    blogs(title, content, author_id)
    VALUES(:title, :content, :author_id);
    `,
    update: `UPDATE blogs SET
    id = :id,
    title = :title,
    content = :content,
    author_id = :author_id,
    created_at = :created_at,
    updated_at = :updated_at,
    deleted = :deleted
    WHERE id = :id 
    AND author_id = :author_id;
    `,
    delete: `
    UPDATE blogs SET
    deleted = 1
    WHERE id = :id
    AND author_id = :author_id;
    `,
    getAll: `SELECT
    id, title, content,author_id, created_at
    FROM blogs
    WHERE deleted = 0
    LIMIT :limit OFFSET :offset;
    `,
    getOne: `SELECT
    id, title, content,author_id, created_at
    FROM blogs
    WHERE id = :id AND 
    deleted = 0;
    `,
    getAllByUser: `SELECT
    id, title, content,author_id, created_at
    FROM blogs
    WHERE author_id = :author_id AND
    deleted = 0
    LIMIT :limit OFFSET :offset;
    `,
    removeAllByUser: `
    UPDATE blogs
    SET deleted = 1
    WHERE author_id = :id;
    `,
    saveImages: `
    INSERT INTO blog_images
    (public_id, blog_id,author_id, url)
    VALUES ?;
    `,
    removeImage: `
    UPDATE blog_images
    SET deleted = 1
    WHERE id = :id
    AND author_id = :author_id;
    `,
    removeImagesByBlog: `
    UPDATE blog_images
    SET deleted = 1
    WHERE blog_id = :blog_id
    AND author_id = :author_id;
    `,
    getImages: `
    SELECT 
    id, public_id, blog_id, url
    FROM blog_images
    WHERE blog_id IN(?)
    AND deleted = 0;
    `,
    getOneImage: `
    SELECT 
    id, public_id, blog_id, url
    FROM blog_images
    WHERE blog_id = :blog_id
    AND deleted = 0;
    `,
};

export default Queries;