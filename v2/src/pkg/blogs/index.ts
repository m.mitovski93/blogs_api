import 'reflect-metadata';
import { Exclude, Expose, plainToClass } from "class-transformer";
import connection from '../db';
import Queries from './query';
import { ResultSetHeader, RowDataPacket } from 'mysql2';
import {
    validate,
    Length,
    IsInt,
    IsDateString,
    Min,
    Max,
    IsArray
} from 'class-validator';
import { queryBuilder } from 'mysql-query-placeholders';

interface IIMAGE {
    id: string,
    public_id: string;
    url: string;
    blog_id: string;
    deleted: number;
};

class Blog {
    @Expose()
    @IsInt({ groups: ['update'] })
    id?: number;

    @Expose()
    @Length(3, 30, { groups: ['create', 'update'] })
    public title: string;

    @Expose()
    @Length(30, 20000, { groups: ['create', 'update'] })
    public content: string;

    @Expose()
    @IsInt({ groups: ['create', 'update'] })
    public author_id: number;

    @Expose()
    @IsArray()
    public images: IIMAGE[] = [];

    @Expose()
    @IsInt({ groups: ['update'] })
    @Min(0)
    @Max(1)
    public deleted: number;

    @Expose()
    @IsDateString({}, { groups: ['update'] })
    public created_at?: Date;

    @Expose()
    @IsDateString({}, { groups: ['update'] })
    public updated_at?: Date;

    constructor(id: number,
        title: string,
        content: string,
        author_id: number,
        images: IIMAGE[],
        created_at: Date,
        updated_at: Date
    ) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.author_id = author_id;
        this.images = images;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.deleted = 0;
    };

    public async create() {
        const queryData = queryBuilder(Queries.create, this);
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public async update() {
        const queryData = queryBuilder(Queries.update, this);
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public static async delete(id: number, author_id: number) {
        const queryData = queryBuilder(Queries.delete, { id, author_id });
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public static async getAll(limit: number, offset: number) {
        const queryData = queryBuilder(Queries.getAll, { limit, offset });
        const [rows] = await connection.promise().query<RowDataPacket[]>(queryData);
        return rows;
    };

    public static async getOne(id: number) {
        const queryData = queryBuilder(Queries.getOne, { id });
        const [rows] = await connection.promise().query<RowDataPacket[]>(queryData);
        return rows;
    };

    public static async getAllByUser(author_id: number, limit: number, offset: number) {
        const queryData = queryBuilder(Queries.getAllByUser, { author_id, limit, offset });
        const [rows] = await connection.promise().query<RowDataPacket[]>(queryData);
        return rows;
    };

    public async saveImages() {
        // bulk insert
        const imageData = this.images.map((i: IIMAGE) => {
            return [i.public_id, this.id, this.author_id, i.url];
        });
        const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.saveImages, [imageData]);
        return rows;
    };

    public static async getImages(images: number[]) {
        const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.getImages, [images]);
        return rows;
    };

    public static async deleteOneImage(id: number, author_id: number) {
        const queryData = queryBuilder(Queries.removeImage, { id, author_id });
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public static async deleteImagesByBlog(blog_id: number, author_id: number) {
        const queryData = queryBuilder(Queries.removeImagesByBlog, { blog_id, author_id });
        const [rows] = await connection.promise().query<ResultSetHeader>(queryData);
        return rows;
    };

    public async validate(groups: string[] = []) {
        return validate(this, {
            groups: groups,
            forbidUnknownValues: true,
            validationError: { target: false }
        }).then(errors => {
            if (errors.length > 0) {
                return errors;
            }
            return null;
        });
    };

    static fromJson(data: Object) {
        return plainToClass(this, data, { excludeExtraneousValues: true });
    };

};

export default Blog;