import mysql, { Connection, ConnectionOptions } from 'mysql2';
import config from '../config';

const cfg = config.get<ConnectionOptions>('db');

const connectOptions: ConnectionOptions = {
    host: cfg.host,
    port: cfg.port,
    user: cfg.user,
    password: cfg.password,
    database: cfg.database,
    connectionLimit: cfg.connectionLimit,
};

let connection: Connection = mysql.createConnection(connectOptions);

export default connection;

