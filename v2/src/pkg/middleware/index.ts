import { Request, Response, NextFunction, Errback } from 'express';
import { verifyToken } from '../security/jwt';
import { UploadedFile } from 'express-fileupload';

export const authenticate = (req: Request, res: Response, next: NextFunction) => {
    try {
        let token: string = '';
        if (req.headers.authorization) {
            token = req.headers.authorization.replace('Bearer ', '');
            const data = verifyToken(token);
            res.locals.user = data;
            next();
        } else {
            return res.status(401).send('Unauthorized');
        }

    } catch (error: any) {
        if (
            error.name === "TokenExpiredError" ||
            error.name === "JsonWebTokenError"
        ) {
            return res.status(401).send("Unauthorized");
        }
        res.status(500).send(error);
        console.log(error);
    };
};

export const filesToArr = (req: Request, res: Response, next: NextFunction) => {
    const doc = req.files?.document;
    if (!doc) {
        return res.status(400).send('No files provided');
    };

    if (Array.isArray(doc)) {
        res.locals.files = doc;
        next();
    } else {
        res.locals.files = [doc];
        next();
    };
};

