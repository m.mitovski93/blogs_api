export interface Services {
    auth: { port: number };
    articles: { port: number };
    storage: { port: number };
}

export interface Security {
    secret: string;
    token_exp: string;
    cookie_exp: number;
};

export interface Storage {
    upload_dir: string;
    max_filesize: number;
    allowed_filetypes: string[];
};

