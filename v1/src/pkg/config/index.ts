import fs from 'fs';
import path from 'path';

const configFile = path.join(__dirname, '../../../config.json');
let config: any;

if (!config) {
    let cf = fs.readFileSync(configFile, "utf-8");
    config = JSON.parse(cf);
}

const get = <T>(section: string): T => {
    return config[section]
};


export default {
    get,
}; 