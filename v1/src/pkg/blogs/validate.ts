import { Validator } from 'node-input-validator';
import { BlogCreate, BlogUpdate, Schema } from './types';

const CREATE = {
    title: "required|minLength:3|maxLength:100|string",
    content: "required|minLength:10|maxLength:20000|string",
};

const UPDATE = {
    title: "minLength:3|maxLength:100|string",
    content: "minLength:10|maxLength:20000|string",
};

type BlogData = BlogCreate | BlogUpdate

async function validate(data: BlogData, schema: string) {
    let sch: any;
    switch (schema) {
        case Schema.CREATE:
            sch = CREATE;
            break;

        case Schema.UPDATE:
            sch = UPDATE;
            break;

        default:
            break;
    }

    let v = new Validator(data, sch);
    let match = await v.check();

    if (!match) {
        throw v.errors;
    }
};

export default validate;