const Queries = {
    create: `INSERT INTO 
    blogs(title, content, author_id)
    VALUES(?, ?, ?)
    `,
    update: `UPDATE blogs SET ?
    WHERE id = ? 
    AND author_id = ?
    `,
    remove: `
    UPDATE blogs SET
    deleted = ?
    WHERE id = ?
    AND author_id = ?
    `,
    getAll: `SELECT
    id, title, content,author_id, created_at
    FROM blogs
    WHERE deleted = ?
    LIMIT ? OFFSET ?
    `,
    getOne: `SELECT
    id, title, content,author_id, created_at
    FROM blogs
    WHERE id = ? AND 
    deleted = ?
    `,
    getAllByUser: `SELECT
    id, title, content,author_id, created_at
    FROM blogs
    WHERE author_id = ? AND
    deleted = ?
    LIMIT ? OFFSET ?
    `,
    removeAllByUser: `
    UPDATE blogs
    SET deleted = ?
    WHERE author_id = ?
    `
};

export default Queries;