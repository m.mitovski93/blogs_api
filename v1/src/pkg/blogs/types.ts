export interface BlogCreate {
    title: string;
    content: string;
    author_id: string;
};

export interface BlogUpdate {
    title?: string;
    content?: string;
};

export enum Schema {
    CREATE = 'CREATE',
    UPDATE = 'UPDATE'
};