import connection from "../db";
import Queries from "./queries";
import { BlogCreate, BlogUpdate } from "./types";
import { ResultSetHeader, RowDataPacket } from 'mysql2';

const create = async (data: BlogCreate) => {
    const params = [data.title, data.content, data.author_id];
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.create, params);
    return rows;
};

const update = async (data: BlogUpdate, id: number, authorId: number) => {
    const params = [data, id, authorId];
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.update, params);
    return rows;
};

const remove = async (id: number, authorId: number) => {
    const params = [1, id, authorId];
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.remove, params);
    return rows;
};

const getAll = async (limit: number, offset: number) => {
    const params = [0, limit, offset]
    const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.getAll, params);
    return rows;
};

const getOne = async (id: number) => {
    const params = [id, 0]
    const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.getOne, params);
    return rows;
};


const getAllByUser = async (id: number, limit: number, offset: number) => {
    const params = [id, 0, limit, offset]
    const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.getAllByUser, params);
    return rows;
};

const removeAllByUser = async (id: number) => {
    const params = [1, id];
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.removeAllByUser, params);
    return rows;
};


export default {
    create,
    update,
    remove,
    getAll,
    getOne,
    getAllByUser,
    removeAllByUser
}