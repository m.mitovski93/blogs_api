import { Validator } from 'node-input-validator';
import { User, UserUpdate, UserLogin, Schema } from './types';

const CREATE: User = {
    first_name: "required|minLength:3|maxLength:20|string",
    last_name: "required|minLength:4|maxLength:20|string",
    email: "required|email",
    password: "required|minLength:8|string",

};

const UPDATE: UserUpdate = {
    first_name: "minLength:3|maxLength:20|string",
    last_name: "minLength:4|maxLength:20|string",
    password: "minLength:8|string",

};

const LOGIN: UserLogin = {
    email: "required|email",
    password: "required",
};

type UserData = User | UserUpdate | UserLogin;

async function validate (data: UserData, schema: string)  {
    let sch: any;
    switch (schema) {
        case Schema.CREATE:
            sch = CREATE;
            break;

        case Schema.UPDATE:
            sch = UPDATE;
            break;

        case Schema.LOGIN:
            sch = LOGIN;
            break;

        default:
            break;
    }

    let v = new Validator(data, sch);
    let match = await v.check();

    if (!match) {
        throw v.errors;
    }
};

export default validate;