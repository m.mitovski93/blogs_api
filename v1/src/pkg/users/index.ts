import connection from '../db';
import Queries from './queries';
import { User, UserUpdate } from './types';
import { ResultSetHeader, RowDataPacket } from 'mysql2';

const create = async (data: User): Promise<ResultSetHeader> => {
    const params = [data.first_name, data.last_name, data.email, data.password];
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.create, params);
    return rows;
};


const getByEmail = async (email: string): Promise<RowDataPacket[]> => {
    const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.getByEmail, [email, 0]);
    return rows;
};

const update = async (id: number, params: UserUpdate): Promise<ResultSetHeader> => {
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.updateById, [params, id, 0]);
    return rows;
};

const getById = async (id: number): Promise<RowDataPacket[]> => {
    const [rows] = await connection.promise().query<RowDataPacket[]>(Queries.getById, [id, 0]);
    return rows;
};

const deleteOne = async (id: string): Promise<ResultSetHeader> => {
    const [rows] = await connection.promise().query<ResultSetHeader>(Queries.deleteById, [id, 0]);
    return rows;
};

export default {
    create,
    getByEmail,
    update,
    getById,
    deleteOne
};

