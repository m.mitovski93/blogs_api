export interface User {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    repeat_password?: string
};

export interface UserUpdate {
    first_name: string;
    last_name: string;
    password: string;
    repeat_password?: string
};

export interface UserLogin {
    email: string;
    password: string;
};

export enum Schema {
    CREATE = 'CREATE',
    UPDATE = 'UPDATE',
    LOGIN = 'LOGIN'
};

