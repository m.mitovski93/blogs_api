const Queries = {
    create: `
    INSERT INTO users
    (first_name, last_name, email, password)
    VALUES(?,?,?,?)
    `,
    getByEmail: `
    SELECT id, first_name, last_name, email, password
    FROM users
    WHERE email = ?
    AND deleted = ?
    `,
    updateById: `
    UPDATE users SET ?
    WHERE id = ?
    AND deleted = ?
    `,
    deleteById: `
    UPDATE users Set
    deleted = 1
    WHERE id = ?
    AND deleted = ?
    `,
    getById: `
    SELECT id, first_name, last_name, email
    FROM users
    WHERE id = ? 
    AND deleted = ?
    `
};

export default Queries;