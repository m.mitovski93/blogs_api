import { Request, Response, NextFunction, Errback } from 'express';
import { verifyToken } from '../security/jwt';

const authenticate = (req: Request, res: Response, next: NextFunction) => {
    try {
        let token: string = '';
        if (req.headers.authorization) {
            token = req.headers.authorization.replace('Bearer ', '');
            const data = verifyToken(token);
            res.locals.user = data;
            next();
        } else {
            return res.status(401).send('Unauthorizes');
        }

    } catch (error: any) {
        if (
            error.name === "TokenExpiredError" ||
            error.name === "JsonWebTokenError"
        ) {
            return res.status(401).send("Unauthorized");
        }
        res.status(500).send(error);
        console.log(error);
    };
};

export default {
    authenticate
};