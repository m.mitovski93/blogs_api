import { Request, Response } from 'express';
import sanitazers from '../../../pkg/sanitazers';
import validate from '../../../pkg/blogs/validate';
import { BlogCreate, BlogUpdate, Schema } from '../../../pkg/blogs/types';
import blogs from '../../../pkg/blogs';

const create = async (req: Request<{}, {}, BlogCreate>, res: Response) => {
    sanitazers.clear(req.body);
    try {
        await validate(req.body, Schema.CREATE);
    } catch (error) {
        return res.status(400).send(error);
    };

    try {
        req.body.author_id = res.locals.user.id;
        const b = await blogs.create(req.body);
        return res.status(201).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };
};

const update = async (req: Request<{ id: string }, {}, BlogUpdate>, res: Response) => {
    sanitazers.clear(req.body);
    try {
        await validate(req.body, Schema.UPDATE);
    } catch (error) {
        return res.status(400).send(error);
    };

    try {
        const author_id: number = res.locals.user.id;
        const blogId = parseInt(req.params.id);
        const updateDataArr: string[] = Object.keys(req.body);
        if (updateDataArr.length === 0) {
            return res.status(400).send('bad request');
        };

        const updateData: BlogUpdate = {};
        if (req.body.content) {
            updateData.content = req.body.content;
        };
        if (req.body.title) {
            updateData.title = req.body.title;
        };
        const b = await blogs.update(req.body, blogId, author_id);
        if (!b.affectedRows) {
            return res.status(400).send('bad request')
        };

        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };
};

const remove = async (req: Request<{ id: string }>, res: Response) => {
    const id: number = res.locals.user.id;
    const blogId: number = parseInt(req.params.id);

    try {
        const b = await blogs.remove(blogId, id);
        if (!b.affectedRows) {
            return res.status(400).send('bad request');
        };

        res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    }
};


const getAll = async (req: Request<{}, {}, {}, { page: string }>, res: Response) => {

    let page: number = parseInt(req.query.page);
    let offset: number;
    let limit: number = 10

    if (page === 1) {
        offset = 0;
    } else {
        offset = (page - 1) * limit;
    };

    try {
        const b = await blogs.getAll(limit, offset);
        return res.status(200).send(b);
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    }
};


const getOne = async (req: Request<{ id: string }>, res: Response) => {
    const postId = parseInt(req.params.id);

    try {
        const b = await blogs.getOne(postId);
        if (!b[0]) {
            return res.status(404).send('Not found');
        };
        return res.status(200).send(b);

    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    };
};

const getAllByUser = async (req: Request<{}, {}, {}, { page: string }>, res: Response) => {

    let page: number = parseInt(req.query.page);
    let offset: number;
    let limit: number = 10

    if (page === 1) {
        offset = 0;
    } else {
        offset = (page - 1) * limit;
    };

    try {
        const userId: number = res.locals.user.id
        const b = await blogs.getAllByUser(userId, limit, offset);
        return res.status(200).send(b);
    } catch (error) {
        console.log(error);
        res.status(400).send(error);
    }
};

export default {
    create,
    update,
    remove,
    getAll,
    getOne,
    getAllByUser
};