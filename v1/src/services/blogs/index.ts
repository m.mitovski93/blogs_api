import express, { Application } from 'express';
import { Services } from '../../../config.types';
import config from '../../pkg/config';
import middleware from '../../pkg/middleware';
import blogs from './handlers/blogs';

const cfg = config.get<Services>('services');

const app: Application = express();
app.use(express.json());
const PORT: number = cfg.articles.port;

app.post('/api/v1/blogs', middleware.authenticate, blogs.create);
app.patch('/api/v1/blogs/:id', middleware.authenticate, blogs.update);
app.delete('/api/v1/blogs/:id', middleware.authenticate, blogs.remove);
app.get('/api/v1/blogs/me', middleware.authenticate, blogs.getAllByUser);
app.get('/api/v1/blogs', blogs.getAll);
app.get('/api/v1/blogs/:id', blogs.getOne);


app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
});