import { RequestHandler, Request, Response } from 'express';
import user from '../../../pkg/users/index';
import blogs from '../../../pkg/blogs';
import validate from '../../../pkg/users/validate';
import { Schema, User, UserLogin, UserUpdate } from '../../../pkg/users/types';
import mysql, { RowDataPacket } from 'mysql2';
import { getUserToken } from '../../../pkg/security/jwt';
import bcrypt from 'bcrypt';


const createAccount: RequestHandler = async (req: Request<{}, {}, User>, res: Response) => {
    if (req.body.password !== req.body.repeat_password) {
        return res.status(400).send('passwords do not match');
    };

    try {
        await validate(req.body, Schema.CREATE);
    } catch (error) {
        return res.status(400).send(error);
    };

    try {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.password, salt);
        req.body.password = hash;

        const u = await user.create(req.body);
        res.status(201).send('ok');
    } catch (error: any) {
        if (error.errno === 1062) {
            return res
                .status(400)
                .send("email already in use");
        };
        console.log(error);
        res.send(error);
    };
};

const login: RequestHandler = async (req: Request<{}, {}, UserLogin>, res: Response) => {

    try {
        await validate(req.body, Schema.LOGIN);
    } catch (error) {
        return res.status(400).send(error);
    };

    try {
        let u = await user.getByEmail(req.body.email);

        const User: mysql.RowDataPacket = u[0];
        if (!User) {
            return res.status(404).send('wrong password or email');
        };

        if (!bcrypt.compareSync(req.body.password, User.password)) {
            return res.status(400).send('wrong password or email');
        };

        const token: string = getUserToken({
            id: User.id,
            first_name: User.first_name,
            last_name: User.last_name,
            email: User.email,
        });

        return res.status(200).send(token);
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    }
};

const partialUpdate: RequestHandler = async (req: Request<{}, {}, UserUpdate>, res: Response) => {
    try {
        await validate(req.body, Schema.UPDATE);
    } catch (error) {
        return res.status(400).send(error);
    };

    try {
        if (req.body.password) {
            if (req.body.password !== req.body.repeat_password) {
                return res.status(400).send({ password: { message: "passwords do not match" } });
            }
            const salt = bcrypt.genSaltSync(10);
            req.body.password = bcrypt.hashSync(req.body.password, salt);
            delete req.body.repeat_password;
        };

        const u = await user.update(res.locals.user.id, req.body);

        if (!u.affectedRows) {
            return res.status(400).send("Bad request");
        };

        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };

};

const getAuthUser: RequestHandler = async (req: Request, res: Response) => {
    const u = await user.getById(res.locals.user.id);

    const User: RowDataPacket = u[0];
    if (!User) {
        return res.status(403).send('Forbidden');
    };

    return res.status(200).send(u);
};

const deleteAccount = async (req: Request, res: Response) => {
    try {
        await user.deleteOne(res.locals.user.id);
        await blogs.removeAllByUser(res.locals.user.id);

        return res.status(204).send('ok');
    } catch (error) {
        console.log(error);
        return res.status(400).send(error);
    };
};

export default {
    createAccount,
    login,
    partialUpdate,
    deleteAccount,
    getAuthUser
};