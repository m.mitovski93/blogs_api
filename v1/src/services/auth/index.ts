import express, { Application } from 'express';
import handlers from '../auth/handlers/auth';
import config from '../../pkg/config';
import { Services } from '../../../config.types';
import middleware from '../../pkg/middleware/index'
const cfg = config.get<Services>('services');

const app: Application = express();
app.use(express.json());
const PORT: number = cfg.auth.port;

app.post('/api/v1/auth', handlers.createAccount);
app.get('/api/v1/auth', middleware.authenticate, handlers.getAuthUser);
app.post('/api/v1/auth/login', handlers.login);
app.patch('/api/v1/auth/update', middleware.authenticate, handlers.partialUpdate);
app.delete('/api/v1/auth/delete', middleware.authenticate, handlers.deleteAccount);


app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`);
});