import express, { Application } from 'express';
import config from '../../pkg/config';
import { Services } from '../../../config.types';
import fileUplaod from "express-fileupload";
import storage from './handlers/storage';

const app: Application = express();
const cfg = config.get<Services>('services');
const PORT: number = cfg.storage.port;
app.use(express.json());
app.use(fileUplaod());

app.post('/api/v1/storage',storage.upload)

app.listen(PORT, () => {
    console.log(`server runnin on port ${PORT}`)
});
